IMAGE=registry.digitalocean.com/keith/hackpage:$1

docker build . -t $IMAGE && \
    # docker-squash "$IMAGE" --tag "$IMAGE" && \
    docker push $IMAGE && \
    kubectl -n hackpage \
    set image ds/hackpage server=$IMAGE

sleep 1
ATTEMPTS=0
ROLLOUT_STATUS_CMD="kubectl rollout status ds/hackpage -n hackpage"
until $ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq 60 ]; do
  $ROLLOUT_STATUS_CMD
  ATTEMPTS=$((ATTEMPTS + 1))
  sleep 1
done

ECHO "Successfully deployed" $1

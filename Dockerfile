FROM node:14-alpine AS builder

WORKDIR /root

RUN apk add make g++

ADD ./package.json ./yarn.lock ./

RUN yarn install

ENV PATH /root/node_modules/.bin:$PATH

COPY ./src ./src
COPY ./public ./public
COPY *.json ./

RUN yarn build

FROM kamackay/nginx:latest

COPY --from=builder /root/build /www/

COPY ./code.txt /www/
COPY ./nginx.conf /etc/nginx/nginx.conf
